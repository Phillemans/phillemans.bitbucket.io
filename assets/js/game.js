let canvas;
let canvasContext;
let posX = 400; //positie slang x-as
let posY = 300; //positie slang Y-as
let bolVX = 0; //bolsnelheid over X-as
let bolVY = 0; //bolsnelheid over Y-as
let speed = 10; //snelheid van de bol overall
let foodX = Math.floor(Math.random() * 790); //random positie van het voedsel bij opstarten x-as
let foodY = Math.floor(Math.random() * 580); //random positie van het voedsel bij opstarten y-as
let total = 0; //hier krijgt het aantal punten de eerste waarde

window.onload = function() {
    window.addEventListener('keydown',doKeyDown,true); //laat hem luisteren naar toetsenbord
    canvas = document.getElementById('gameCanvas');    //koppelt het variabel canvas aan het canvas
    canvasContext = canvas.getContext('2d');
    let FramesPerSecond = 30;                          //geeft het Frames/seconden aan
    setInterval(function() {                           //dit zorgt ervoor dat binnen het interval alles opnieuw getekend wordt
        drawEverything();
        drawMovemement();
        ifHit();
    }, 1000/FramesPerSecond);
}

function drawEverything(){      //dit tekent alles op het scherm
    // canvas achtergrond
    colorRect(0,0,canvas.width,canvas.height,'black');

    //bol
    colorCircle(posX,posY,10,'red');

    //voedsel
    colorRect(foodX,foodY,20,20,'green');
    
    
    
}

function doKeyDown(evt){        //dit geeft aan wat er gebeurt als je drukt op je wasd toetsen
    switch (evt.keyCode) {
    case 87:  /* w was pressed */
        bolVX = 0;
        bolVY = -speed;
        break;
    case 83:  /* s was pressed */
        bolVX = 0;
        bolVY = speed;
        break;
    case 65:  /* a was pressed */
        bolVX = -speed;
        bolVY = 0;
        break;
    case 68:  /* d was pressed */
        bolVX = speed;
        bolVY = 0;
        break;
    }
}

function ifHit(){ //functie voor als het voedsel geraakt word
    var dist = Math.sqrt( Math.pow((foodX+10-posX), 2) + Math.pow((foodY+10-posY), 2) );//dit berekend de afstand tussen voedsel en de bal vanaf het cecntrum
    if(dist < 20) { //hoe groot de afstand tussen het centrum van het voedsel en de bal is als hij hem raakt
        foodX = Math.floor(Math.random() * 780);
        foodY = Math.floor(Math.random() * 580);
        total++; //telt een punt op bij het totaal al het voedsel geraakt word
        console.log(total); //geeft het totaal weer in console
        document.getElementById("punten").innerHTML = "Je hebt "+total+" punt(en)"; //laat het totaal aantal punten zien in html
    }
}

function drawMovemement(){ //dit zorgt voor de beweging
    posX = posX + bolVX; //positie is positie plus de snelheid
    if(posX > canvas.width ){ //als hij tegen de borders aankomt aan de andere kant verder, dit geld voor alle if's in deze functie
        posX = 0;
    }
    if(posX < 0){
        posX = canvas.width;
    }
    posY = posY + bolVY;
    if(posY > canvas.height){
        posY = 0;
    }
    if(posY < 0){
        posY = canvas.height;
    }
}

function colorCircle(centerX, centerY, radius, color) {     //maakt de bal
    canvasContext.fillStyle = color;
    canvasContext.beginPath();
    canvasContext.arc(centerX, centerY, radius, 0,Math.PI*2, true);
    canvasContext.fill();
}
function colorRect(leftX,topY,width,height,color){          //maakt het vierkant
    canvasContext.fillStyle = color;
    canvasContext.fillRect(leftX,topY,width,height);
}